import PropTypes from 'prop-types';

import Card from './Card';
import PlayerStatus from './PlayerStatus';

class Player {
  static fromBlob(blob) {
    return new Player({
      ...blob,
      interferenceField: blob.interferenceField.map(Card.fromBlob),
      cards: blob.cards.map(Card.fromBlob),
      status: PlayerStatus.from(blob.status),
    });
  }

  constructor({
    id,
    name,
    credits,
    status,
    interferenceField,
    cards,
  }) {
    this.id = id;
    this.name = name;
    this.credits = credits;
    this.status = status;
    this.interferenceField = interferenceField;
    this.cards = cards;
  }

  static get shape() {
    return PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      credits: PropTypes.number.isRequired,
      status: PlayerStatus.shape.isRequired,
      interferenceField: PropTypes.arrayOf(Card.shape).isRequired,
      cards: PropTypes.arrayOf(Card.shape).isRequired,
    });
  }
}

export default Player;
