import PropTypes from 'prop-types';

class User {
  static fromBlob(blob) {
    return new User(blob);
  }

  constructor({ id, name }) {
    this.id = id;
    this.name = name;
  }

  static get shape() {
    return PropTypes.shape({
      name: PropTypes.string,
    })
  }
}

export default User;
