import PropTypes from 'prop-types';

class PlayerStatus {
  static from(value) {
    return new PlayerStatus(value);
  }

  constructor(value) {
    this.value = value;
  }

  static get shape() {
    return PropTypes.shape({
      value: PropTypes.oneOf([
        'InGame',
      ]),
    });
  }
}

export default PlayerStatus;
