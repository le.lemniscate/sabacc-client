import PropTypes from 'prop-types';

import Card from './Card';
import PlayerStatus from './PlayerStatus';

class Opponent {
  static fromBlob(blob) {
    return new Opponent({
      ...blob,
      interferenceField: blob.interferenceField.map(Card.fromBlob),
      status: PlayerStatus.from(blob.status),
    });
  }

  constructor({
    id,
    name,
    credits,
    status,
    interferenceField,
    handSize,
  }) {
    this.id = id;
    this.name = name;
    this.credits = credits;
    this.status = status;
    this.interferenceField = interferenceField;
    this.handSize = handSize;
  }

  static get shape() {
    return PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      credits: PropTypes.number.isRequired,
      status: PlayerStatus.shape.isRequired,
      interferenceField: PropTypes.arrayOf(Card.shape).isRequired,
      handSize: PropTypes.number.isRequired,
    });
  }
}

export default Opponent;
