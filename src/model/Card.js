import PropTypes from 'prop-types';

class Card {
  static fromBlob(blob) {
    return new Card(blob);
  }

  constructor({
    suit,
    value,
    name,
    type,
  }) {
    this.suit = suit;
    this.name = name;
    this.value = value;
    this.type = type;
  }

  static get shape() {
    return PropTypes.shape({
      suit: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      value: PropTypes.number.isRequired,
      type: PropTypes.string.isRequired,
    });
  }
}

export default Card;
