import PropTypes from 'prop-types';
import User from './User';

class Lobby {
  static fromBlob(blob) {
    return new Lobby(blob);
  }

  static from(id) {
    return new Lobby({
      id,
      users: [],
    });
  }

  constructor({ id, users }) {
    this.id = id;
    this.users = users;
  }

  static get shape() {
    return PropTypes.shape({
      id: PropTypes.string,
      users: PropTypes.arrayOf(User.shape),
    })
  }
}

export default Lobby;
