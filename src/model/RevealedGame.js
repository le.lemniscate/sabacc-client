import PropTypes from 'prop-types';

import Player from './Player';

class RevealedGame {
  static fromBlob(blob) {
    return new RevealedGame({
      ...blob,
      player: Player.fromBlob(blob.player),
      opponents: blob.opponents.map(Player.fromBlob),
      deckSize: blob.deck.size,
    });
  }

  constructor({
    id,
    player,
    opponents,
    round,
    phase,
    currentPlayer,
    startingPlayer,
    mainPot,
    sabaccPot,
    ruleset,
    deckSize,
    currentBet,
  }) {
    this.id = id;
    this.player = player;
    this.opponents = opponents;
    this.round = round;
    this.phase = phase;
    this.currentPlayer = currentPlayer;
    this.startingPlayer = startingPlayer;
    this.mainPot = mainPot;
    this.sabaccPot = sabaccPot;
    this.ruleset = ruleset;
    this.deckSize = deckSize;
    this.currentBet = currentBet;
  }

  static get shape() {
    return PropTypes.shape({
      id: PropTypes.string.isRequired,
      player: Player.shape.isRequired,
      opponents: PropTypes.arrayOf(Player.shape).isRequired,
      round: PropTypes.number.isRequired,
      phase: PropTypes.string.isRequired,
      currentPlayer: PropTypes.string.isRequired,
      startingPlayer: PropTypes.string.isRequired,
      mainPot: PropTypes.number.isRequired,
      sabaccPot: PropTypes.number.isRequired,
      ruleset: PropTypes.string.isRequired,
      deckSize: PropTypes.number.isRequired,
      currentBet: PropTypes.number.isRequired,
    });
  }

}

export default RevealedGame;
