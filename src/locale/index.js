// import { addLocaleData } from 'react-intl';

import en from './en.json';

/* import locale_en from 'react-intl/locale-data/en';

addLocaleData([...locale_en]); */

const messages = {
  en,
};

export default messages;
