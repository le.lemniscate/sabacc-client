import { PLAYER_JOINED_LOBBY } from '../lobby/lobbyAction';

export function players(state = {}, action) {
  switch (action.type) {
    case PLAYER_JOINED_LOBBY:
      return {
        ...state,
        [action.payload.player.id]: action.payload.player,
      };
    default:
      return state;
  }
}

export default players;
