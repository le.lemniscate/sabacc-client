export const JOIN_GAME = 'JOIN_GAME';
export const CALL = 'CALL';
export const DRAW = 'DRAW';
export const FOLD = 'FOLD';
export const MATCH_BET = 'MATCH_BET';
export const PUT_IN_FIELD = 'PUT_IN_FIELD';
export const RAISE = 'RAISE';
export const REMOVE_FROM_FIELD = 'REMOVE_FROM_FIELD';
export const STAND = 'STAND';
export const TRADE = 'TRADE';

export const INITIATE_PUT_IN_FIELD = 'INITIATE_PUT_IN_FIELD';
export const INITIATE_REMOVE_FROM_FIELD = 'INITIATE_REMOVE_FROM_FIELD';
export const INITIATE_RAISE_BET = 'INITIATE_RAISE_BET';
export const INITIATE_TRADE = 'INITIATE_TRADE';

export const READY_FOR_SUDDEN_DEMISE = 'READY_FOR_SUDDEN_DEMISE';
export const READY_FOR_REVEAL = 'READY_FOR_REVEAL';

export const CANCEL = 'CANCEL';

export const cancelAction = () => ({
  type: CANCEL,
  payload: {},
});

export const loadGame = (user, gameId) => ({
  type: JOIN_GAME,
  payload: {
    user,
    gameId,
  },
});

export const callHand = (user, game) => ({
  type: CALL,
  payload: {
    gameId: game.id,
  },
});

export const drawCard = (user, game) => ({
  type: DRAW,
  payload: {
    gameId: game.id,
  },
});

export const fold = (user, game) => ({
  type: FOLD,
  payload: {
    gameId: game.id,
  },
});

export const matchBet = (user, game) => ({
  type: MATCH_BET,
  payload: {
    gameId: game.id,
  },
});

export const putInField = (user, game, card) => ({
  type: PUT_IN_FIELD,
  payload: {
    gameId: game.id,
    ...card,
  },
});

export const raiseBet = (user, game, amount) => ({
  type: RAISE,
  payload: {
    gameId: game.id,
    amount,
  },
});

export const removeFromField = (user, game, card) => ({
  type: REMOVE_FROM_FIELD,
  payload: {
    gameId: game.id,
    ...card,
  },
});

export const stand = (user, game) => ({
  type: STAND,
  payload: {
    gameId: game.id,
  },
});

export const trade = (user, game, card) => ({
  type: TRADE,
  payload: {
    gameId: game.id,
    ...card,
  },
});

export const initiatePutInField = () => ({
  type: INITIATE_PUT_IN_FIELD,
  payload: {},
});

export const initiateRemoveFromField = () => ({
  type: INITIATE_REMOVE_FROM_FIELD,
  payload: {},
});

export const initiateRaiseBet = () => ({
  type: INITIATE_RAISE_BET,
  payload: {},
});

export const initiateTrade = () => ({
  type: INITIATE_TRADE,
  payload: {},
});

export const readyForSuddenDemise = (game) => ({
  type: READY_FOR_SUDDEN_DEMISE,
  payload: {
    gameId: game.id,
  },
});

export const readyForReveal = (game) => ({
  type: READY_FOR_REVEAL,
  payload: {
    gameId: game.id,
  },
});
