export function getCurrentGame(state) {
  return state.currentGame;
}

export function getRevealedGame(state) {
  return state.revealedGame;
}

export function isCurrentPlayer(player) {
  return (state) => getCurrentGame(state).currentPlayer === player.id;
}
