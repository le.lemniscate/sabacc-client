export const HAND_CALLED = 'HAND_CALLED';
export const PLAYER_CALLED_HAND = 'PLAYER_CALLED_HAND';
export const CARD_DREW = 'CARD_DREW';
export const PLAYER_DREW_CARD = 'PLAYER_DREW_CARD';
export const FOLDED = 'FOLDED';
export const PLAYER_FOLDED = 'PLAYER_FOLDED';
export const BET_MATCHED = 'BET_MATCHED';
export const PLAYER_MATCHED_BET = 'PLAYER_MATCHED_BET';
export const CARD_PUT_IN_FIELD = 'CARD_PUT_IN_FIELD';
export const PLAYER_PUT_CARD_IN_FIELD = 'PLAYER_PUT_CARD_IN_FIELD';
export const BET_RAISED = 'BET_RAISED';
export const PLAYER_RAISED_BET = 'PLAYER_RAISED_BET';
export const CARD_REMOVED_FROM_FIELD = 'CARD_REMOVED_FROM_FIELD';
export const PLAYER_REMOVED_CARD_FROM_FIELD = 'PLAYER_REMOVED_CARD_FROM_FIELD';
export const STOOD = 'STOOD';
export const PLAYER_STOOD = 'PLAYER_STOOD';
export const CARD_TRADED = 'CARD_TRADED';
export const PLAYER_TRADED_CARD = 'PLAYER_TRADED_CARD';
export const ENTERRING_SUDDEN_DEMISE = 'ENTERRING_SUDDEN_DEMISE';
export const GAME_OVER = 'GAME_OVER';

export const READY_FOR_SUDDEN_DEMISE_ACK = 'READY_FOR_SUDDEN_DEMISE_ACK';
export const READY_FOR_REVEAL_ACK = 'READY_FOR_REVEAL_ACK';
export const PLAYER_READY_FOR_SUDDEN_DEMISE = 'PLAYER_READY_FOR_SUDDEN_DEMISE';
export const PLAYER_READY_FOR_REVEAL = 'PLAYER_READY_FOR_REVEAL';


export const handCalled = (game) => ({
  type: HAND_CALLED,
  payload: {
    game,
  },
});

export const playerCalledHand = (game) => ({
  type: PLAYER_CALLED_HAND,
  payload: {
    game,
  },
});

export const cardDrew = (game) => ({
  type: CARD_DREW,
  payload: {
    game,
  },
});

export const playerDrewCard = (game) => ({
  type: PLAYER_DREW_CARD,
  payload: {
    game,
  },
});

export const folded = (game) => ({
  type: FOLDED,
  payload: {
    game,
  },
});

export const playerFolded = (game) => ({
  type: PLAYER_FOLDED,
  payload: {
    game,
  },
});

export const betMatched = (game) => ({
  type: BET_MATCHED,
  payload: {
    game,
  },
});

export const playerMatchedBet = (game) => ({
  type: PLAYER_MATCHED_BET,
  payload: {
    game,
  },
});

export const cardPutInField = (game) => ({
  type: CARD_PUT_IN_FIELD,
  payload: {
    game,
  },
});

export const playerPutCardInField = (game) => ({
  type: PLAYER_PUT_CARD_IN_FIELD,
  payload: {
    game,
  },
});

export const betRaised = (game) => ({
  type: BET_RAISED,
  payload: {
    game,
  },
});

export const playerRaisedBet = (game) => ({
  type: PLAYER_RAISED_BET,
  payload: {
    game,
  },
});

export const cardRemovedFromField = (game) => ({
  type: CARD_REMOVED_FROM_FIELD,
  payload: {
    game,
  },
});

export const playerRemovedCardFromField = (game) => ({
  type: PLAYER_REMOVED_CARD_FROM_FIELD,
  payload: {
    game,
  },
});

export const stood = (game) => ({
  type: STOOD,
  payload: {
    game,
  },
});

export const playerStood = (game) => ({
  type: PLAYER_STOOD,
  payload: {
    game,
  },
});

export const cardTraded = (game) => ({
  type: CARD_TRADED,
  payload: {
    game,
  },
});

export const playerTradedCard = (game) => ({
  type: PLAYER_TRADED_CARD,
  payload: {
    game,
  },
});

export const readyForRevealAck = (playerId) => ({
  type: READY_FOR_REVEAL_ACK,
  payload: {
    playerId,
  },
})

export const readyForSuddenDemiseAck = (playerId) => ({
  type: READY_FOR_SUDDEN_DEMISE_ACK,
  payload: {
    playerId,
  },
})

export const playerReadyForReveal = (playerId) => ({
  type: PLAYER_READY_FOR_REVEAL,
  payload: {
    playerId,
  },
})

export const playerReadyForSuddenDemise = (playerId) => ({
  type: PLAYER_READY_FOR_SUDDEN_DEMISE,
  payload: {
    playerId,
  },
})

export const enterringSuddenDemise = (game) => ({
  type: ENTERRING_SUDDEN_DEMISE,
  payload: {
    game,
  },
});

export const gameOver = (game) => ({
  type: GAME_OVER,
  payload: {
    game,
  },
});
