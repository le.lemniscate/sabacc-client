import { map, ignoreElements } from 'rxjs/operators';
import { ofType } from 'redux-observable';

import ws from '../../services/WebSocketService';
import { CALL, DRAW, FOLD, MATCH_BET, PUT_IN_FIELD, RAISE, REMOVE_FROM_FIELD, STAND, TRADE, JOIN_GAME, READY_FOR_REVEAL, READY_FOR_SUDDEN_DEMISE } from './gameAction';
import { SOCKET_URL } from '../../config';

export const loadGameEpic = action$ =>
  action$.pipe(
    ofType(JOIN_GAME),
    map(action =>
      ws.connect(`${SOCKET_URL}?id=${action.payload.user.id}`).then((service) => {
        service.send({ ...action, payload: { gameId: action.payload.gameId }})
      })
    ),
    ignoreElements(),
  );

export const callEpic = action$ =>
  action$.pipe(
    ofType(CALL),
    map(action =>
      ws.send(action)
    ),
    ignoreElements(),
  );

export const drawEpic = action$ =>
  action$.pipe(
    ofType(DRAW),
    map(action =>
      ws.send(action)
    ),
    ignoreElements(),
  );

export const foldEpic = action$ =>
  action$.pipe(
    ofType(FOLD),
    map(action =>
      ws.send(action)
    ),
    ignoreElements(),
  );

export const matchBetEpic = action$ =>
  action$.pipe(
    ofType(MATCH_BET),
    map(action =>
      ws.send(action)
    ),
    ignoreElements(),
  );

export const putInFieldEpic = action$ =>
  action$.pipe(
    ofType(PUT_IN_FIELD),
    map(action =>
      ws.send(action)
    ),
    ignoreElements(),
  );

export const raiseEpic = action$ =>
  action$.pipe(
    ofType(RAISE),
    map(action =>
      ws.send(action)
    ),
    ignoreElements(),
  );

export const removeFromFieldEpic = action$ =>
  action$.pipe(
    ofType(REMOVE_FROM_FIELD),
    map(action =>
      ws.send(action)
    ),
    ignoreElements(),
  );

export const standEpic = action$ =>
  action$.pipe(
    ofType(STAND),
    map(action =>
      ws.send(action)
    ),
    ignoreElements(),
  );

export const tradeEpic = action$ =>
  action$.pipe(
    ofType(TRADE),
    map(action =>
      ws.send(action)
    ),
    ignoreElements(),
  );

export const readyForRevealEpic = action$ =>
  action$.pipe(
    ofType(READY_FOR_REVEAL),
    map(action =>
      ws.send(action)
    ),
    ignoreElements(),
  );

export const readyForSuddenDemiseEpic = action$ =>
  action$.pipe(
    ofType(READY_FOR_SUDDEN_DEMISE),
    map(action =>
      ws.send(action)
    ),
    ignoreElements(),
  );
