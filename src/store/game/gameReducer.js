import { GAME_LAUNCHED } from '../lobby/lobbyAction';
import {
  HAND_CALLED,
  PLAYER_CALLED_HAND,
  CARD_DREW,
  PLAYER_DREW_CARD,
  FOLDED,
  PLAYER_FOLDED,
  BET_MATCHED,
  PLAYER_MATCHED_BET,
  CARD_PUT_IN_FIELD,
  PLAYER_PUT_CARD_IN_FIELD,
  BET_RAISED,
  PLAYER_RAISED_BET,
  CARD_REMOVED_FROM_FIELD,
  PLAYER_REMOVED_CARD_FROM_FIELD,
  STOOD,
  PLAYER_STOOD,
  CARD_TRADED,
  PLAYER_TRADED_CARD,
  GAME_OVER,
  ENTERRING_SUDDEN_DEMISE,
} from './gameReaction';

export function currentGame(state = null, action) {
  switch (action.type) {
    case GAME_LAUNCHED:
    case HAND_CALLED:
    case PLAYER_CALLED_HAND:
    case CARD_DREW:
    case PLAYER_DREW_CARD:
    case FOLDED:
    case PLAYER_FOLDED:
    case BET_MATCHED:
    case PLAYER_MATCHED_BET:
    case CARD_PUT_IN_FIELD:
    case PLAYER_PUT_CARD_IN_FIELD:
    case BET_RAISED:
    case PLAYER_RAISED_BET:
    case CARD_REMOVED_FROM_FIELD:
    case PLAYER_REMOVED_CARD_FROM_FIELD:
    case STOOD:
    case PLAYER_STOOD:
    case CARD_TRADED:
    case PLAYER_TRADED_CARD:
      return action.payload.game;
    default:
      return state;
  }
}

export function revealedGame(state = null, action) {
  switch (action.type) {
    case ENTERRING_SUDDEN_DEMISE:
    case GAME_OVER:
      console.log('action ', action);
      return action.payload.game;
    default:
      return state;
  }
}
