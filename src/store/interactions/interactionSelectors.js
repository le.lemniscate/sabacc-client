import { INITIATE_PUT_IN_FIELD, INITIATE_TRADE, INITIATE_REMOVE_FROM_FIELD, removeFromField, trade, putInField, INITIATE_RAISE_BET } from '../game/gameAction';
import { doNothing } from '../../utils/propTypes';
import { GAME_OVER } from '../game/gameReaction';

export function isPutInInterferenceFieldOngoing(state) {
  return state.cardSelectionOngoing === INITIATE_PUT_IN_FIELD;
}

export function isRemoveFromInterferenceFieldOngoing(state) {
  return state.cardSelectionOngoing === INITIATE_REMOVE_FROM_FIELD;
}

export function isTradeOngoing(state) {
  return state.cardSelectionOngoing === INITIATE_TRADE;
}

export function isHandInSelectionMode(state) {
  return isPutInInterferenceFieldOngoing(state)
    || isTradeOngoing(state);
}

export function isInterferenceFieldInSelectionMode(state) {
  return isRemoveFromInterferenceFieldOngoing(state);
}

export function getCardSelectionAction(state) {
  switch (state.cardSelectionOngoing) {
    case INITIATE_PUT_IN_FIELD:
      return putInField;
    case INITIATE_REMOVE_FROM_FIELD:
      return removeFromField;
    case INITIATE_TRADE:
      return trade;
    default:
      return doNothing;
  }
}

export function isRaiseBetOngoing(state) {
  return state.displayedPopIn === INITIATE_RAISE_BET;
}

export function isGameOver(state) {
  return state.displayedPopIn === GAME_OVER;
}
