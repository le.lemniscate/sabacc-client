import { INITIATE_TRADE, INITIATE_PUT_IN_FIELD, INITIATE_REMOVE_FROM_FIELD, INITIATE_RAISE_BET, CANCEL } from '../game/gameAction';
import { CARD_PUT_IN_FIELD, CARD_REMOVED_FROM_FIELD, CARD_TRADED, BET_RAISED, GAME_OVER } from '../game/gameReaction';

export function cardSelectionOngoing(state = null, action) {
  switch (action.type) {
    case INITIATE_PUT_IN_FIELD:
    case INITIATE_REMOVE_FROM_FIELD:
    case INITIATE_TRADE:
      return action.type;
    case CARD_PUT_IN_FIELD:
    case CARD_REMOVED_FROM_FIELD:
    case CARD_TRADED:
    case CANCEL:
      return null;
    default:
      return state;
  }
}

export function displayedPopIn(state = null, action) {
  switch (action.type) {
    case INITIATE_RAISE_BET:
      return action.type;
    case GAME_OVER:
      console.log('Game OVER!');
      return action.type;
    case BET_RAISED:
    case CANCEL:
      return null;
    default:
      return state;
  }
}
