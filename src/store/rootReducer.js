import { combineReducers } from 'redux';
import { combineEpics } from 'redux-observable';

import { joinedLobby, lobbies } from './lobby/lobbyReducer';
import { players } from './players/playerReducer';
import { currentGame, revealedGame } from './game/gameReducer';
import { cardSelectionOngoing, displayedPopIn } from './interactions/interactionsReducer';
import {
  getAllLobbiesEpic,
  createLobbyEpic,
  joinLobbyEpic,
  launchGameEpic,
} from './lobby/lobbyEpics';
import { callEpic, foldEpic, drawEpic, matchBetEpic, putInFieldEpic, raiseEpic, removeFromFieldEpic, standEpic, tradeEpic, loadGameEpic, readyForRevealEpic, readyForSuddenDemiseEpic } from './game/gameEpics';

export const rootEpic = combineEpics(
  getAllLobbiesEpic,
  createLobbyEpic,
  joinLobbyEpic,
  launchGameEpic,
  callEpic,
  drawEpic,
  foldEpic,
  matchBetEpic,
  putInFieldEpic,
  raiseEpic,
  removeFromFieldEpic,
  standEpic,
  tradeEpic,
  loadGameEpic,
  readyForRevealEpic,
  readyForSuddenDemiseEpic,
);

export const rootReducer = combineReducers({
  lobbies,
  joinedLobby,
  players,
  currentGame,
  revealedGame,
  cardSelectionOngoing,
  displayedPopIn,
});
