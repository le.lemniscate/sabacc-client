export const PLAYER_JOINED_LOBBY = 'PLAYER_JOINED_LOBBY';

export const REQUEST_LOBBIES = 'REQUEST_LOBBIES';
export const CREATE_LOBBY = 'CREATE_LOBBY';

export const LOBBIES_RECEIVED = 'LOBBIES_RECEIVED';
export const LOBBY_CREATED = 'LOBBY_CREATED';

export const JOIN_LOBBY = 'JOIN_LOBBY';
export const LOBBY_JOINED = 'LOBBY_JOINED';

export const LAUNCH_GAME = 'LAUNCH_GAME';
export const GAME_LAUNCHED = 'GAME_LAUNCHED';

export const LOBBY_UPDATED = 'LOBBY_UPDATED';

export const lobbyUpdated = lobby => ({
  type: LOBBY_UPDATED,
  payload: {
    lobby,
  },
});

export const playerJoinedLobby = player => ({
  type: PLAYER_JOINED_LOBBY,
  payload: {
    player,
  },
});

export const requestLobbies = () => ({
  type: REQUEST_LOBBIES,
});

export const createLobby = (user, allowance = 2000) => ({
  type: CREATE_LOBBY,
  payload: {
    user,
    allowance,
  },
});

export const joinLobby = (user, lobby) => ({
  type: JOIN_LOBBY,
  payload: {
    user,
    lobby,
  },
});

export const lobbiesReceived = lobbies => ({
  type: LOBBIES_RECEIVED,
  payload: {
    lobbies,
  }
});

export const lobbyCreated = lobby => ({
  type: LOBBY_CREATED,
  payload: {
    lobby,
  },
});

export const lobbyJoined = lobby => ({
  type: LOBBY_JOINED,
  payload: {
    lobby,
  },
});

export const launchGame = (user, lobby) => ({
  type: LAUNCH_GAME,
  payload: {
    user,
    lobby,
  },
});

export const gameLaunched = game => ({
  type: GAME_LAUNCHED,
  payload: {
    game,
  },
});
