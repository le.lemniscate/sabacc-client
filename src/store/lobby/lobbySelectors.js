export function getPeopleInLobby(state, lobbyId) {
  return state.lobbies[lobbyId].users;
}

export function getLobbies(state) {
  if (state.lobbies) {
    return Object.values(state.lobbies);
  }
  return null;
}

export function getLobby(id) {
  return (state) => {
    if (state.lobbies) {
      return state.lobbies[id];
    }
    return null;
  }
}

export function getJoinedLobby(state) {
  return state.joinedLobby;
}
