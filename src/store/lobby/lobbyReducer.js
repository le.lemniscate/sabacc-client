import { PLAYER_JOINED_LOBBY, LOBBIES_RECEIVED, LOBBY_JOINED, LOBBY_CREATED, LOBBY_UPDATED } from './lobbyAction';

export function lobbies(state = null, action) {
  switch (action.type) {
    case LOBBIES_RECEIVED:
      return action.payload.lobbies.reduce((acc, lobby) => {
        acc[lobby.id] = lobby;
        return acc;
      }, {});
    case PLAYER_JOINED_LOBBY:
    case LOBBY_UPDATED:
    case LOBBY_CREATED:
    case LOBBY_JOINED: {
      const { lobby } = action.payload;
      return ({
        ...state,
        [lobby.id]: lobby,
      });
    }
    default:
      return state;
  }
}

export function joinedLobby(state = null, action) {
  switch (action.type) {
    case LOBBY_JOINED:
      return action.payload.lobby.id;
    case LOBBY_UPDATED:
    case LOBBY_CREATED:
      return action.payload.lobby.id;
    default:
      return state;
  }
}
