import { from } from 'rxjs';
import { mergeMap, map, ignoreElements } from 'rxjs/operators';
import { ofType } from 'redux-observable';

import { REQUEST_LOBBIES, lobbiesReceived, CREATE_LOBBY, JOIN_LOBBY, LAUNCH_GAME } from './lobbyAction';
import LobbyRepository from '../../services/LobbyRepository';

export const getAllLobbiesEpic = action$ =>
  action$.pipe(
    ofType(REQUEST_LOBBIES),
    mergeMap(() =>
      from(LobbyRepository.getAll()).pipe(
        map(lobbiesReceived)
      ))
  );

export const createLobbyEpic = action$ =>
  action$.pipe(
    ofType(CREATE_LOBBY),
    map(action =>
      LobbyRepository.create(action.payload)
    ),
    ignoreElements(),
  );

export const joinLobbyEpic = action$ =>
  action$.pipe(
    ofType(JOIN_LOBBY),
    map(action =>
      LobbyRepository.join(action.payload)
    ),
    ignoreElements(),
  );

export const launchGameEpic = action$ =>
  action$.pipe(
    ofType(LAUNCH_GAME),
    map(action =>
      LobbyRepository.launch(action.payload)
    ),
    ignoreElements(),
  );