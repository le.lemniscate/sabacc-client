import React, { useContext } from 'react';

import { anyNumberOfChildren } from '../utils/propTypes';
import AuthService from '../services/AuthService';

const AuthContext = React.createContext({
  user: undefined,
  login: () => {},
  logout: () => {},
});

export class AuthContextProvider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: undefined,
    };
    this.setUser = this.setUser.bind(this);
    this.login = this.login.bind(this);
  }

  componentDidMount() {
    const storedUsername = AuthService.storedSession();
    if (storedUsername) {
      this.login(storedUsername);
    }
  }

  setUser(user) {
    this.setState({ user });
  }

  login(username) {
    this.setUser(null);
    AuthService
      .login(username)
      .then(this.setUser)
      .catch(() => {
        AuthService.register(username)
          .then(this.setUser)
      });
  }

  render() {
    const { user } = this.state;
    const { children } = this.props;
    return (
      <AuthContext.Provider
        value={{
          user,
          login: this.login,
          logout: () => {
            this.setUser(undefined);
          }
        }}
      >
        {children}
      </AuthContext.Provider>
    )
  }
};

AuthContextProvider.propTypes = {
  children: anyNumberOfChildren.isRequired,
};

export const useAuth = () => useContext(AuthContext);
