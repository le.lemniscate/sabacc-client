import PropTypes from 'prop-types';

export const anyNumberOfChildren = PropTypes.oneOfType([
  PropTypes.arrayOf(PropTypes.node),
  PropTypes.node,
]);

export const doNothing = () => {};
