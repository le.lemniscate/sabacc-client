import React from 'react';
import { IntlProvider } from 'react-intl';
import { Provider } from 'react-redux';
import styled from 'styled-components';

import SabaccRouter from './router';
import configureStore from './store';
import { AuthContextProvider } from './hooks/AuthProvider';
import locale from './locale';

const MainStyle = styled.div`
  text-align: center;
  margin: 16px 24px 16px 16px;
`;

function App() {
  const language = 'en';
  return (
    <MainStyle>
      <Provider store={configureStore()}>
        <AuthContextProvider>
          <IntlProvider
            locale={language}
            messages={locale[language]}
          >
            <SabaccRouter />
          </IntlProvider>
        </AuthContextProvider>
      </Provider>
    </MainStyle>
  );
}

export default App;
