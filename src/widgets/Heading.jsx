import React from 'react';

import { anyNumberOfChildren } from '../utils/propTypes';

const Heading = ({ children }) => (
  <p className="heading">{children}</p>
);

Heading.propTypes = {
  children: anyNumberOfChildren.isRequired,
};

export default Heading;
