import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { anyNumberOfChildren } from '../utils/propTypes';

const Subtitle = ({ children, marginless }) => (
  <p className={clsx(
    'subtitle',
    { 'is-marginless': marginless },
  )}>
    {children}
  </p>
);

Subtitle.propTypes = {
  children: anyNumberOfChildren.isRequired,
  marginless: PropTypes.bool,
};

Subtitle.defaultProps = {
  marginless: false,
};

export default Subtitle;
