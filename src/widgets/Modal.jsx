import React from 'react'
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { anyNumberOfChildren } from '../utils/propTypes';

const Modal = ({ children, isActive, onClose }) => (
  <div className={clsx(
    'modal',
    { 'is-active': isActive },
  )}>
    <div className="modal-background" />
    <div className="modal-content">
      {children}
    </div>
    <button
      type="button"
      className="modal-close is-large"
      onClick={onClose}
      aria-label="close"
    />
  </div>
);

Modal.propTypes = {
  children: anyNumberOfChildren.isRequired,
  isActive: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
};

Modal.defaultProps = {
  isActive: false,
};

export default Modal;
