import React from 'react';
import { anyNumberOfChildren } from '../utils/propTypes';

const Columns = ({ children }) => (
  <div className="columns">{children}</div>
);

Columns.propTypes = {
  children: anyNumberOfChildren.isRequired,
};

export default Columns;
