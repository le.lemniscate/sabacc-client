import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { anyNumberOfChildren } from '../utils/propTypes';

const LevelItem = ({
  children,
  hasTextCentered,
}) => (
  <div
    className={clsx(
      'level-item',
      { 'has-text-centered': hasTextCentered }
    )}
  >
    {children}
  </div>
);

LevelItem.propTypes = {
  children: anyNumberOfChildren.isRequired,
  hasTextCentered: PropTypes.bool,
};

LevelItem.defaultProps = {
  hasTextCentered: false,
};

export default LevelItem;
