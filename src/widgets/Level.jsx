import React from 'react';

import { anyNumberOfChildren } from '../utils/propTypes';

const Level = ({ children }) => (
  <div className="level">{children}</div>
);

Level.propTypes = {
  children: anyNumberOfChildren.isRequired,
};

export default Level;
