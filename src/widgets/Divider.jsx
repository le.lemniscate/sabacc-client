import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import clsx from 'clsx';

const Style = styled.div`
    display: table; 
    text-align: center; 
    width: 75%;
    margin: 32px auto;
    margin-left: auto !important;
    margin-right: auto !important;

    > span { display: table-cell; position: relative; }

    > span:first-child, > span:last-child {
      width: 50%;
      top: 13px;
      -moz-background-size: 100% 2px;
      background-size: 100% 2px;
      background-position: 0 0, 0 100%;
      background-repeat: no-repeat;
    }

    > span:first-child {
      background-image: -webkit-gradient(linear, 0 0, 0 100%, from(transparent), to(#000));
      background-image: -webkit-linear-gradient(180deg, transparent, #000);
      background-image: -moz-linear-gradient(180deg, transparent, #000);
      background-image: -o-linear-gradient(180deg, transparent, #000);
      background-image: linear-gradient(90deg, transparent, #000);
    }

    > span:nth-child(2) {
      color: #000; padding: 0px 5px; width: auto; white-space: nowrap;
    }

    > span:last-child {
      background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#000), to(transparent));
      background-image: -webkit-linear-gradient(180deg, #000, transparent);
      background-image: -moz-linear-gradient(180deg, #000, transparent);
      background-image: -o-linear-gradient(180deg, #000, transparent);
      background-image: linear-gradient(90deg, #000, transparent);
    }
`;

const Divider = ({ children, marginless }) => (
  <Style className={clsx(
    { 'is-marginless': marginless }
  )}>
    <span />
    <span><FormattedMessage id={children} /></span>
    <span />
  </Style>
);

Divider.propTypes = {
  children: PropTypes.string.isRequired,
  marginless: PropTypes.bool,
};

Divider.defaultProps = {
  marginless: false,
};

export default Divider;
