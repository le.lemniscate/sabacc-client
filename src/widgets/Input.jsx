import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { doNothing } from '../utils/propTypes';

export const TextInput = injectIntl(({ placeholder, intl, onChange }) => {
  const placeholderIntl = intl.formatMessage({ id: placeholder });
  return (
    <input
      className="input"
      type="text"
      placeholder={placeholderIntl}
      onChange={e => onChange(e.target.value)}
    />
  );
});

TextInput.propTypes = {
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
};

TextInput.defaultProps = {
  placeholder: '',
  onChange: doNothing,
};

export default TextInput;
