import React from 'react';
import PropTypes from 'prop-types';

class Loader extends React.Component {
  componentDidMount() {
    const { load } = this.props;
    load();
  }

  render() {
    return (
      <img
        src="https://i.gifer.com/8p7m.gif"
        alt="Loading"
      />
    );
  }
}

Loader.propTypes = {
  load: PropTypes.func.isRequired,
};

export default Loader;
