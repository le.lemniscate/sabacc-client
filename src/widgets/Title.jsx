import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { anyNumberOfChildren } from '../utils/propTypes';

const Title = ({ children, marginless }) => (
  <p className={clsx(
    'title',
    { 'is-marginless': marginless },
  )}>
    {children}
  </p>
);

Title.propTypes = {
  children: anyNumberOfChildren.isRequired,
  marginless: PropTypes.bool,
};

Title.defaultProps = {
  marginless: false,
};

export default Title;
