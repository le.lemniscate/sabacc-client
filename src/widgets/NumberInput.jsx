import React, { useState } from 'react';
import PropTypes from 'prop-types';

const NumberInput = ({ min, onChange }) => {
  const [value, setValue] = useState(min);
  return (
    <input
      className="input"
      type="number"
      min={min}
      value={value}
      onChange={e => {
        setValue(e.target.value);
        onChange(e.target.value);
      }}
    />
  );
}

NumberInput.propTypes = {
  min: PropTypes.number,
  onChange: PropTypes.func.isRequired,
};

NumberInput.defaultProps = {
  min: 0,
};

export default NumberInput;
