import React from 'react';
import { anyNumberOfChildren } from '../utils/propTypes';

const Column = ({ children }) => (
  <div className="column">{children}</div>
);

Column.propTypes = {
  children: anyNumberOfChildren.isRequired,
};

export default Column;
