import React from 'react';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import clsx from 'clsx';

const Button = ({
  onClick,
  children,
  disabled,
  selected,
  success,
  primary,
}) => (
  <button
    type="button"
    className={clsx(
      'button',
      {
        'is-focused': selected,
        'is-success': success,
        'is-primary': primary,
      },
    )}
    onClick={onClick}
    disabled={disabled}
    selected={selected}
  >
    <FormattedMessage id={children} />
  </button>
);

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  children: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  selected: PropTypes.bool,
  success: PropTypes.bool,
  primary: PropTypes.bool,
};

Button.defaultProps = {
  disabled: false,
  selected: false,
  success: false,
  primary: false,
};

export default Button;
