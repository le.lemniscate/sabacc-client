import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { anyNumberOfChildren } from '../utils/propTypes';

const Tile = ({
  children,
  ancestor,
  vertical,
  parent,
  child,
}) => (
  <div className={clsx(
    'tile',
    {
      'is-ancestor': ancestor,
      'is-vertical': vertical,
      'is-parent': parent,
      'is-child': child,
    },
  )}>
    {children}
  </div>
);

Tile.propTypes = {
  children: anyNumberOfChildren.isRequired,
  ancestor: PropTypes.bool,
  vertical: PropTypes.bool,
  parent: PropTypes.bool,
  child: PropTypes.bool,
};

Tile.defaultProps = {
  ancestor: false,
  vertical: false,
  parent: false,
  child: false,
};

export default Tile;
