import React from 'react'
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { FormattedMessage } from 'react-intl';

import { anyNumberOfChildren } from '../utils/propTypes';
import Button from './Button';

const ModalCard = ({
  children,
  isActive,
  onClose,
  onValidate,
  title,
}) => (
  <div className={clsx(
    'modal',
    { 'is-active': isActive },
  )}>
    <div className="modal-background" />
    <div className="modal-card">
      <header className="modal-card-head">
        <p className="modal-card-title">
          <FormattedMessage id={title} />
        </p>
        <button
          type="button"
          className="delete"
          aria-label="close"
          onClick={onClose}
        />
      </header>
      <section className="modal-card-body">
        {children}
      </section>
      <footer className="modal-card-foot">
        <Button success onClick={onValidate}>bet</Button>
        <Button onClick={onClose}>cancel</Button>
      </footer>
    </div>
    <button
      type="button"
      className="modal-close is-large"
      onClick={onClose}
      aria-label="close"
    />
  </div>
);

ModalCard.propTypes = {
  children: anyNumberOfChildren.isRequired,
  isActive: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  onValidate: PropTypes.func.isRequired,
};

ModalCard.defaultProps = {
  isActive: false,
};

export default ModalCard;
