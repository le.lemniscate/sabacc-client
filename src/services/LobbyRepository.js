import axios from './axios';
import Lobby from '../model/Lobby';
import ws from './WebSocketService';
import { SOCKET_URL } from '../config';

class LobbyRepository {
  static getAll() {
    return axios.get('/lobby')
      .then(res => res.data.map(Lobby.fromBlob));
  }

  static create({ user, allowance }) {
    return ws.connect(`${SOCKET_URL}?id=${user.id}`).then((service) => {
      service.send({
        type: 'CREATE_LOBBY',
        payload: {
          userId: user.id,
          allowance
        },
      });
    });
  }

  static join({ user, lobby }) {
    return ws.connect(`${SOCKET_URL}?id=${user.id}`).then((service) => {
      service.send({
        type: 'JOIN_LOBBY',
        payload: {
          userId: user.id,
          lobbyId: lobby.id,
        },
      })
    });
  }

  static launch({ user, lobby }) {
    return ws.send({
      type: 'LAUNCH_GAME',
      payload: {
        userId: user.id,
        lobbyId: lobby.id,
      },
    });
  }
}

export default LobbyRepository;
