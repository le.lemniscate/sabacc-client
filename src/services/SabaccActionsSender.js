const CALL = 'CALL';
const DRAW = 'DRAW';
const FOLD = 'FOLD';
const MATCH_BET = 'MATCH_BET';
const PUT_IN_INTERFERENCE_FIELD = 'PUT_IN_INTERFERENCE_FIELD';
const RAISE_BET = 'RAISE_BET';
const REMOVE_FROM_INTERFERENCE_FIELD = 'REMOVE_FROM_INTERFERENCE_FIELD';
const STAND = 'STAND';
const TRADE = 'TRADE';

const CREATE_LOBBY = 'CREATE_LOBBY';
const JOIN_LOBBY = 'JOIN_LOBBY';
const START_GAME = 'START_GAME';

class SabaccActionsSender {
  constructor(webSocketService) {
    this.socket = webSocketService;
  }

  callHand(game, player) {
    this.socket.send({
      type: CALL,
      payload: {
        game,
        player,
      },
    });
  }

  draw(game, player) {
    this.socket.send({
      type: DRAW,
      payload: {
        game,
        player,
      },
    });
  }

  fold(game, player) {
    this.socket.send({
      type: FOLD,
      payload: {
        game,
        player,
      },
    });
  }

  matchBet(game, player) {
    this.socket.send({
      type: MATCH_BET,
      payload: {
        game,
        player,
      },
    });
  }

  putInInterferenceField(game, player, card) {
    this.socket.send({
      type: PUT_IN_INTERFERENCE_FIELD,
      payload: {
        game,
        player,
        card,
      },
    });
  }

  raiseBetTo(game, player, target) {
    this.socket.send({
      type: RAISE_BET,
      payload: {
        game,
        player,
        target,
      },
    });
  }

  removeFromInterferenceField(game, player, card) {
    this.socket.send({
      type: REMOVE_FROM_INTERFERENCE_FIELD,
      payload: {
        game,
        player,
        card,
      },
    });
  }

  stand(game, player) {
    this.socket.send({
      type: STAND,
      payload: {
        game,
        player,
      },
    });
  }

  trade(game, player, card) {
    this.socket.send({
      type: TRADE,
      payload: {
        game,
        player,
        card,
      },
    });
  }

  createLobby(player) {
    this.socket.send({
      type: CREATE_LOBBY,
      payload: {
        player,
      },
    });
  }

  joinLobby(lobby, player) {
    this.socket.send({
      type: JOIN_LOBBY,
      payload: {
        player,
      },
    });
  }

  startGame(lobby) {
    this.socket.send({
      type: START_GAME,
      payload: {
        lobby,
      },
    });
  }

}

export default SabaccActionsSender;
