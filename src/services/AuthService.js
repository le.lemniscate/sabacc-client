import axios from './axios';
import User from '../model/User';

const LOGIN_STORAGE = 'login_storage';

class AuthService {
  static register(username) {
    return axios.post('/register', {
      username
    }).then(res => {
      localStorage.setItem(LOGIN_STORAGE, username);
      return User.fromBlob(res.data);
    });
  }

  static login(username) {
    return axios.post('/login', {
      username
    }).then(res => {
      localStorage.setItem(LOGIN_STORAGE, username);
      return User.fromBlob(res.data)
    });
  }

  static storedSession() {
    return localStorage.getItem(LOGIN_STORAGE);
  }
}

export default AuthService;
