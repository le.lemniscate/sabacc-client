import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { lobbyUpdated, lobbyJoined, gameLaunched } from '../store/lobby/lobbyAction';
import { anyNumberOfChildren } from '../utils/propTypes';
import Lobby from '../model/Lobby';
import Game from '../model/Game';
import {
  HAND_CALLED,
  PLAYER_CALLED_HAND,
  CARD_DREW,
  PLAYER_DREW_CARD,
  FOLDED,
  PLAYER_FOLDED,
  BET_MATCHED,
  PLAYER_MATCHED_BET,
  CARD_PUT_IN_FIELD,
  PLAYER_PUT_CARD_IN_FIELD,
  BET_RAISED,
  PLAYER_RAISED_BET,
  CARD_REMOVED_FROM_FIELD,
  PLAYER_REMOVED_CARD_FROM_FIELD,
  STOOD,
  PLAYER_STOOD,
  CARD_TRADED,
  PLAYER_TRADED_CARD,
  handCalled,
  playerCalledHand,
  cardDrew,
  playerDrewCard,
  folded,
  playerFolded,
  betMatched,
  playerMatchedBet,
  cardPutInField,
  playerPutCardInField,
  betRaised,
  playerRaisedBet,
  cardRemovedFromField,
  playerRemovedCardFromField,
  stood,
  playerStood,
  cardTraded,
  playerTradedCard,
  readyForRevealAck,
  readyForSuddenDemiseAck,
  playerReadyForReveal,
  playerReadyForSuddenDemise,
  PLAYER_READY_FOR_SUDDEN_DEMISE,
  PLAYER_READY_FOR_REVEAL,
  READY_FOR_SUDDEN_DEMISE_ACK,
  READY_FOR_REVEAL_ACK,
  gameOver,
  enterringSuddenDemise,
} from '../store/game/gameReaction';
import RevealedGame from '../model/RevealedGame';

const PLAYER_LAUNCHED_GAME = 'PLAYER_LAUNCHED_GAME';
const GAME_LAUNCHED = 'GAME_LAUNCHED';

const LOBBY_CREATED = 'LOBBY_CREATED';
const PLAYER_JOINED_LOBBY = 'PLAYER_JOINED_LOBBY';
const PLAYER_LEFT_LOBBY = 'PLAYER_LEFT_LOBBY';
const LOBBY_JOINED = 'LOBBY_JOINED';
const LOBBY_LEFT = 'LOBBY_LEFT';

const GAME_JOINED = 'GAME_JOINED';

const SUDDEN_DEMISE = 'SUDDEN_DEMISE';
const REVEAL = 'REVEAL';

// TODO
// const PLAYER_JOINED_GAME = 'PLAYER_JOINED_GAME';
// /TODO

const LOBBY_ERROR = 'LOBBY_ERROR';
const GAME_ERROR = 'GAME_ERROR';


const SabaccEventReceiver = ({ webSocketService, children }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  useEffect(() => {
    webSocketService.addListener({
      onMessage: (msg) => {
        console.log('Received: ', msg);
        switch (msg.type) {
          case LOBBY_CREATED:
          case PLAYER_JOINED_LOBBY:
          case PLAYER_LEFT_LOBBY: {
            const lobby = Lobby.fromBlob(msg.payload);
            dispatch(lobbyUpdated(lobby));
            break;
          }
          case LOBBY_JOINED: {
            const lobby = Lobby.fromBlob(msg.payload);
            history.push(`/lobby/${lobby.id}`);
            dispatch(lobbyJoined(lobby));
            break;
          }
          case LOBBY_LEFT:
            break;
          case GAME_JOINED: {
            const game = Game.fromBlob(msg.payload);
            dispatch(gameLaunched(game));
            break;
          }
          case PLAYER_LAUNCHED_GAME:
          case GAME_LAUNCHED: {
            const game = Game.fromBlob(msg.payload);
            history.push(`/game`);
            dispatch(gameLaunched(game));
            break;
          }
          case HAND_CALLED: {
            const game = Game.fromBlob(msg.payload);
            dispatch(handCalled(game));
            break;
          }
          case PLAYER_CALLED_HAND: {
            const game = Game.fromBlob(msg.payload);
            dispatch(playerCalledHand(game));
            break;
          }
          case CARD_DREW: {
            const game = Game.fromBlob(msg.payload);
            dispatch(cardDrew(game));
            break;
          }
          case PLAYER_DREW_CARD: {
            const game = Game.fromBlob(msg.payload);
            dispatch(playerDrewCard(game));
            break;
          }
          case FOLDED: {
            const game = Game.fromBlob(msg.payload);
            dispatch(folded(game));
            break;
          }
          case PLAYER_FOLDED: {
            const game = Game.fromBlob(msg.payload);
            dispatch(playerFolded(game));
            break;
          }
          case BET_MATCHED: {
            const game = Game.fromBlob(msg.payload);
            dispatch(betMatched(game));
            break;
          }
          case PLAYER_MATCHED_BET: {
            const game = Game.fromBlob(msg.payload);
            dispatch(playerMatchedBet(game));
            break;
          }
          case CARD_PUT_IN_FIELD: {
            const game = Game.fromBlob(msg.payload);
            dispatch(cardPutInField(game));
            break;
          }
          case PLAYER_PUT_CARD_IN_FIELD: {
            const game = Game.fromBlob(msg.payload);
            dispatch(playerPutCardInField(game));
            break;
          }
          case BET_RAISED: {
            const game = Game.fromBlob(msg.payload);
            dispatch(betRaised(game));
            break;
          }
          case PLAYER_RAISED_BET: {
            const game = Game.fromBlob(msg.payload);
            dispatch(playerRaisedBet(game));
            break;
          }
          case CARD_REMOVED_FROM_FIELD: {
            const game = Game.fromBlob(msg.payload);
            dispatch(cardRemovedFromField(game));
            break;
          }
          case PLAYER_REMOVED_CARD_FROM_FIELD: {
            const game = Game.fromBlob(msg.payload);
            dispatch(playerRemovedCardFromField(game));
            break;
          }
          case STOOD: {
            const game = Game.fromBlob(msg.payload);
            dispatch(stood(game));
            break;
          }
          case PLAYER_STOOD: {
            const game = Game.fromBlob(msg.payload);
            dispatch(playerStood(game));
            break;
          }
          case CARD_TRADED: {
            const game = Game.fromBlob(msg.payload);
            dispatch(cardTraded(game));
            break;
          }
          case PLAYER_TRADED_CARD: {
            const game = Game.fromBlob(msg.payload);
            dispatch(playerTradedCard(game));
            break;
          }
          case READY_FOR_REVEAL_ACK:
            dispatch(readyForRevealAck());
            break;
          case READY_FOR_SUDDEN_DEMISE_ACK:
            dispatch(readyForSuddenDemiseAck());
            break;
          case PLAYER_READY_FOR_SUDDEN_DEMISE:
            dispatch(playerReadyForSuddenDemise(msg.payload.value));
            break;
          case PLAYER_READY_FOR_REVEAL:
            dispatch(playerReadyForReveal(msg.payload.value));
            break;
          case SUDDEN_DEMISE:
            dispatch(enterringSuddenDemise(RevealedGame.fromBlob(msg.payload)));
            break;
          case REVEAL:
            dispatch(gameOver(RevealedGame.fromBlob(msg.payload)));
            break;
          case LOBBY_ERROR:
          case GAME_ERROR:
            break;
          default:
            break;
        }
      },
    });
  }, [webSocketService, dispatch, history])

  return (<>{children}</>);
};

SabaccEventReceiver.propTypes = {
  children: anyNumberOfChildren.isRequired,
  webSocketService: PropTypes.shape(PropTypes.any).isRequired,
};

export default SabaccEventReceiver;
