class WebSocketService {
  constructor() {
    this.openPromise = null;
    this.listeners = [];
    this.messageReceived = this.messageReceived.bind(this);
  }

  connect(url) {
    if (this.openPromise === null) {
      this.openPromise = new Promise((resolve) => {
        this.socket = new WebSocket(url);
        this.socket.onopen = () => resolve(this);
        this.socket.onclose = () => { this.openPromise = null };
        this.socket.onmessage = this.messageReceived;
      });
      return this.openPromise;
    }
    return this.openPromise;
  }

  messageReceived(msg) {
    this.listeners.forEach(element => {
      element.onMessage(JSON.parse(msg.data))
    });
  }

  send(json) {
    this.socket.send(JSON.stringify(json));
  }

  addListener(listener) {
    this.listeners.push(listener);
  }

  removeListener(listener) {
    this.listeners = this.listeners.filter(element => element !== listener);
  }

}

export default new WebSocketService();