export const SOCKET_URL = process.env.REACT_SOCKET_URL || 'ws://localhost:8080/ws/sabacc';
export const API_URL = process.env.REACT_API_URL || 'http://localhost:8080';
