import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';

import Loader from '../../widgets/Loader';
import { createLobby } from '../../store/lobby/lobbyAction';
import { useAuth } from '../../hooks/AuthProvider';
import { getJoinedLobby } from '../../store/lobby/lobbySelectors';

const CreateLobby = () => {
  const dispatch = useDispatch();
  const { user } = useAuth();
  const joinedLobby = useSelector(getJoinedLobby);
  return joinedLobby === null
      ? (<Loader load={() => dispatch(createLobby(user))} />)
      : (<Redirect to={`/lobby/${joinedLobby}`} />)
  ;
}

export default CreateLobby;
