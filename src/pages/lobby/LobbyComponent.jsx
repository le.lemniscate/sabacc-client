import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import Lobby from '../../model/Lobby';
import UserList from './UserList';
import Button from '../../widgets/Button';
import { launchGame } from '../../store/lobby/lobbyAction';
import { useAuth } from '../../hooks/AuthProvider';

const LobbyComponent = ({ lobby }) => {
  console.log('lobby: ', lobby);
  const dispatch = useDispatch();
  const history = useHistory();
  const { user } = useAuth();
  return (
    <>
      <div>Welcome to lobby {lobby.id}</div>
      <UserList users={lobby.users} />
      <Button
        onClick={() => {
          history.push('/game');
          dispatch(launchGame(user, lobby))
        }}
      >
        lobby.launch.game
      </Button>
    </>
  );
}

LobbyComponent.propTypes = {
  lobby: Lobby.shape.isRequired,
};

export default LobbyComponent;
