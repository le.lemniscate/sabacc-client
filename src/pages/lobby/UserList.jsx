import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import User from '../../model/User';

const UserList = ({ users }) => (
  <>
    <FormattedMessage id="lobby.users" />
    <ul>
      {users.map(user => (
        <li>{user.name}</li>
      ))}
    </ul>
  </>
);

UserList.propTypes = {
  users: PropTypes.arrayOf(User.shape).isRequired,
};

export default UserList;
