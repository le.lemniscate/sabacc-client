import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';

import Loader from '../../widgets/Loader';
import LobbyComponent from './LobbyComponent';
import { joinLobby } from '../../store/lobby/lobbyAction';
import { getLobby } from '../../store/lobby/lobbySelectors';
import { useAuth } from '../../hooks/AuthProvider';
import Lobby from '../../model/Lobby';

const LobbyLoader = () => {
  const { id } = useParams();
  const { user } = useAuth();
  const history = useHistory();
  const dispatch = useDispatch();
  const lobby = useSelector(getLobby(id))
  if (lobby === null) {
    return (<Loader load={() => dispatch(joinLobby(user, Lobby.from(id), history))} />);
  }
  return (<LobbyComponent lobby={lobby} />);
};

export default LobbyLoader;
