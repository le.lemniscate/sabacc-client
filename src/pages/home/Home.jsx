import React from 'react';
import { useHistory } from 'react-router-dom';

import { useAuth } from '../../hooks/AuthProvider';
import Button from '../../widgets/Button';
import LobbiesLoader from './LobbiesLoader';

const Home = () => {
  const { user } = useAuth();
  const history = useHistory();
  return (
    <div className="container">
      <div>Welcome, {user.name}</div>
      <Button
        onClick={() => history.push('/lobby/create')}
      >
        lobby.create
      </Button>
      <LobbiesLoader />
    </div>
  );
};

export default Home;
