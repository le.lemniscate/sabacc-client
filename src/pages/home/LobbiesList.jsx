import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { useDispatch } from 'react-redux';

import Lobby from '../../model/Lobby';
import Button from '../../widgets/Button';
import { joinLobby } from '../../store/lobby/lobbyAction';
import { useAuth } from '../../hooks/AuthProvider';

const LobbiesList = ({ lobbies }) => {
  const { user } = useAuth();
  const dispatch = useDispatch();
  return (
    <>
      <div><FormattedMessage id="existing.lobbies" /></div>
      <ul>
        {lobbies.map(lobby => (
          <li>
            {lobby.id}
            <Button onClick={() => dispatch(joinLobby(user, lobby))}>lobby.join</Button>
          </li>
        ))}
      </ul>
    </>
  );
}

LobbiesList.propTypes = {
  lobbies: PropTypes.arrayOf(Lobby.shape).isRequired,
};

export default LobbiesList;
