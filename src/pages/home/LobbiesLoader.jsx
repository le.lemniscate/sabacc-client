import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Loader from '../../widgets/Loader';
import LobbiesList from './LobbiesList';
import { getLobbies } from '../../store/lobby/lobbySelectors';
import { requestLobbies } from '../../store/lobby/lobbyAction';

const LobbiesLoader = () => {
  const lobbies = useSelector(getLobbies);
  const dispatch = useDispatch();
  if (lobbies === null) {
    return (<Loader load={() => dispatch(requestLobbies())} />);
  }
  return (
    <LobbiesList
      lobbies={lobbies}
    />
  );
}

export default LobbiesLoader;
