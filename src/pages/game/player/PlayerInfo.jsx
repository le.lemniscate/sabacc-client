import React from 'react';
import { useSelector } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import Player from '../../../model/Player';
import { isCurrentPlayer } from '../../../store/game/gameSelectors';
import Heading from '../../../widgets/Heading';
import PlayerName from '../misc/PlayerName';

const PlayerInfo = ({ player }) => {
  const hasTurn = useSelector(isCurrentPlayer(player));
  return (
    <div>
      <PlayerName name={player.name} isPlayerTurn={hasTurn} />
      <Heading>
        <span>
          <FormattedMessage
            id={`status.${player.status.value.toLowerCase()}`}
          />
        </span>
        <span style={{margin: '6pt'}}>-</span>
        <span>
          <i className="swg swg-credits" />
          <FormattedMessage
            id="opponent.credits"
            values={{
              amount: player.credits
            }}
          />
        </span>
      </Heading>
      <Heading>
        <FormattedMessage
          id="player.score"
          values={{
            amount: [...player.cards, ...player.interferenceField].reduce((acc, card) =>
              acc + card.value, 0),
          }}
        />
      </Heading>
    </div>
  );
}

PlayerInfo.propTypes = {
  player: Player.shape.isRequired,
};

export default PlayerInfo;
