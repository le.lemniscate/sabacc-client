import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { uuid } from 'uuidv4';

import Card from '../../../model/Card';
import { CardView } from '../misc/CardView';
import { isHandInSelectionMode, getCardSelectionAction } from '../../../store/interactions/interactionSelectors';
import { useAuth } from '../../../hooks/AuthProvider';
import { getCurrentGame } from '../../../store/game/gameSelectors';

const PlayerHand = ({ cards }) => {
  const user = useAuth();
  const isInSelectionMode = useSelector(isHandInSelectionMode);
  const selectionAction = useSelector(getCardSelectionAction);
  const game = useSelector(getCurrentGame);
  const dispatch = useDispatch();
  return (
    <div>
      {cards.map(card => {
        const onClick = isInSelectionMode
          ? () => dispatch(selectionAction(user, game, card))
          : undefined;
        return (
            <CardView
              card={card}
              onClick={onClick}
              key={uuid()}
            />
        );
      })}
    </div>
  );
};

PlayerHand.propTypes = {
  cards: PropTypes.arrayOf(Card.shape).isRequired,
};

export default PlayerHand;
