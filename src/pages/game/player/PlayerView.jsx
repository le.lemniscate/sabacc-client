import React from 'react';

import PlayerInfo from './PlayerInfo';
import Player from '../../../model/Player';
import Tile from '../../../widgets/Tile';
import PlayerCards from './PlayerCards';

const PlayerView = ({ player }) => (
  <Tile ancestor vertical>
    <PlayerInfo player={player} />
    <PlayerCards player={player} />
  </Tile>
);

PlayerView.propTypes = {
  player: Player.shape.isRequired,
};

export default PlayerView;
