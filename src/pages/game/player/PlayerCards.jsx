import React from 'react';
import Tile from '../../../widgets/Tile';
import Player from '../../../model/Player';
import InterferenceField from '../misc/InterferenceField';
import PlayerHand from './PlayerHand';

const PlayerCards = ({ player }) => (
  <Tile child>
    <Tile parent>
      <Tile child>
        <PlayerHand cards={player.cards} />
      </Tile>
      <Tile child>
        <InterferenceField cards={player.interferenceField} />
      </Tile>
    </Tile>
  </Tile>
);

PlayerCards.propTypes = {
  player: Player.shape.isRequired,
};

export default PlayerCards;