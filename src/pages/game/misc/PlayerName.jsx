import React from 'react';
import PropTypes from 'prop-types';

import Title from '../../../widgets/Title';

const PlayerName = ({ name, isPlayerTurn }) => (
  <Title marginless>
    {isPlayerTurn ? <i className="swg swg-decals-4" /> : undefined}
    {name}
  </Title>
);

PlayerName.propTypes = {
  name: PropTypes.string.isRequired,
  isPlayerTurn: PropTypes.bool.isRequired,
};

export default PlayerName;
