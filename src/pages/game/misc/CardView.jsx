import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import Card from '../../../model/Card';

function getImage(card) {
  if (card.type === 'FACE') {
    return `${process.env.PUBLIC_URL}/images/${card.name.toLowerCase()}.jpg`;
  }
  return `${process.env.PUBLIC_URL}/images/${card.suit.toLowerCase()}_${card.value}.jpg`;
}

const CardStyle = styled.img`
  height: 140pt;
`;

const CardSelectionStyle = styled.img`
  height: 140pt;
  
  -webkit-transition: all 0.30s ease-in-out;
  -moz-transition: all 0.30s ease-in-out;
  -ms-transition: all 0.30s ease-in-out;
  -o-transition: all 0.30s ease-in-out;
  outline: none;
  padding: 3px 0px 3px 3px;
  margin: 5px 1px 3px 0px;
  border: 1px solid #DDDDDD;
  cursor: pointer;

  &:hover {
    box-shadow: 0 0 5px rgba(81, 203, 238, 1);
    padding: 3px 0px 3px 3px;
    margin: 5px 1px 3px 0px;
    border: 1px solid rgba(81, 203, 238, 1);
  }
`;

export const CardView = ({ card, onClick }) => {
  return (onClick
    ? (
      <CardSelectionStyle
        src={getImage(card)}
        alt={`Card[${card.name}, ${card.suit}]`}
        onClick={onClick}
      />
    ) : (
      <CardStyle
        src={getImage(card)}
        alt={`Card[${card.name}, ${card.suit}]`}
      />
    )
  );
};

CardView.propTypes = {
  card: Card.shape.isRequired,
  onClick: PropTypes.func,
};

CardView.defaultProps = {
  onClick: undefined,
};

export const FlippedCardView = () => (
  <CardStyle src={`${process.env.PUBLIC_URL}/images/card_back.png`} alt="Flipped card" />
);

export default CardView;
