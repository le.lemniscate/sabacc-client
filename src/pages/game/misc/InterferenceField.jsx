import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

import Card from '../../../model/Card';
import { CardView } from './CardView';
import { useAuth } from '../../../hooks/AuthProvider';
import { isInterferenceFieldInSelectionMode, getCardSelectionAction } from '../../../store/interactions/interactionSelectors';
import { getCurrentGame } from '../../../store/game/gameSelectors';

const LabelBox = styled.div`
  border: #3c5a86 1px solid;
  height: 100%;

  > .box-title {
    position: relative;
    top: -0.5em;
    margin-left: 1em;
    display: inline;
    background-color: white;
  }
`;

const InterferenceField = ({ cards }) => {
  const user = useAuth();
  const isInSelectionMode = useSelector(isInterferenceFieldInSelectionMode);
  const selectionAction = useSelector(getCardSelectionAction);
  const game = useSelector(getCurrentGame);
  const dispatch = useDispatch();
  return (
    <LabelBox>
      <span
        className="box-title is-pulled-left"
      >
        <FormattedMessage id="action.interference" />
      </span>
      {cards.map(card => {
        const onClick = isInSelectionMode
          ? () => dispatch(selectionAction(user, game, card))
          : undefined;
        return (
          <CardView
            card={card}
            onClick={onClick}
          />
        )
      })}
    </LabelBox>
  );
};

InterferenceField.propTypes = {
  cards: PropTypes.arrayOf(Card.shape).isRequired,
};

export default InterferenceField;
