import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import { FlippedCardView } from './CardView';
import { Stack, Row } from '../../../widgets/Layouts';

const DeckSide = styled.img`
  height: 140pt;
  margin-left: -2pt;
`;

const DeckView = ({ size }) => (
  <Stack>
    <Row>
      <FlippedCardView />
      <DeckSide
        src={`${process.env.PUBLIC_URL}/images/card_deck.png`}
        alt={`Deck has ${size} cards remaining`}
      />
    </Row>
    <FormattedMessage
        id="deck.cards.remaining"
        values={{
          amount: size,
        }}
      />
  </Stack>
);

DeckView.propTypes = {
  size: PropTypes.number.isRequired,
};

export default DeckView;
