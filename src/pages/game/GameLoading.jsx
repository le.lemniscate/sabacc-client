import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { getCurrentGame } from '../../store/game/gameSelectors';
import { doNothing } from '../../utils/propTypes';
import Loader from '../../widgets/Loader';

const GameLoading = () => {
  const currentGame = useSelector(getCurrentGame);
  if (currentGame === null) {
    return (<Loader load={doNothing} />);
  }
  return (<Redirect to={`/game/${currentGame.id}`} />);
};

export default GameLoading;
