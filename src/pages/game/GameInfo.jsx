import React from 'react';
import { FormattedMessage } from 'react-intl';

import Game from '../../model/Game';
import Level from '../../widgets/Level';
import LevelItem from '../../widgets/LevelItem';
import Title from '../../widgets/Title';
import Heading from '../../widgets/Heading';

const GameInfo = ({ game }) => (
  <Level>
    <LevelItem>
      <div>
        <Heading>
          <FormattedMessage id="game.phase" />
        </Heading>
        <Title>
          <FormattedMessage id={`game.phase.${game.phase.toLowerCase()}`} />
        </Title>
        {game.currentBet !== undefined && (
          <div>
            <FormattedMessage
              id="game.current.bet"
              values={{
                amount: game.currentBet
              }}
            />
          </div>
        )}
      </div>
    </LevelItem>
    <LevelItem>
      <div>
        <Heading>
          <FormattedMessage id="game.round" />
        </Heading>
        <Title>
          {game.round}
        </Title>
      </div>
    </LevelItem>
    <LevelItem>
      <div>
        <Heading>
          <FormattedMessage id="game.main.pot" />
        </Heading>
        <Title>
        <i className="swg swg-credits" />{game.mainPot}
        </Title>
      </div>
    </LevelItem>
    <LevelItem>
      <div>
        <Heading>
          <FormattedMessage id="game.sabacc.pot" />
        </Heading>
        <Title>
        <i className="swg swg-credits" />{game.sabaccPot}
        </Title>
      </div>
    </LevelItem>
  </Level>
);

GameInfo.propTypes = {
  game: Game.shape.isRequired,
};

export default GameInfo;
