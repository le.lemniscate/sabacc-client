import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import Button from '../../widgets/Button';
import { getCurrentGame, isCurrentPlayer } from '../../store/game/gameSelectors';
import { callHand, drawCard, fold, matchBet, stand, initiatePutInField, initiateRemoveFromField, initiateRaiseBet, initiateTrade, cancelAction, readyForSuddenDemise, readyForReveal } from '../../store/game/gameAction';
import { useAuth } from '../../hooks/AuthProvider';
import Divider from '../../widgets/Divider';
import { isPutInInterferenceFieldOngoing, isRemoveFromInterferenceFieldOngoing, isTradeOngoing } from '../../store/interactions/interactionSelectors';

const Stack = styled.div`
  display: flex;
  flex-direction: column;
  > button {
    margin-top 8px;
  }
`;

const callAction = 'call';
const drawAction = 'draw';
const foldAction = 'foldAction';
const matchAction = 'match';
const putInFieldAction = 'putInField';
const removeFromFieldAction = 'removeFromField';
const raiseAction = 'raise';
const standAction = 'stand';
const tradeAction = 'trade';

class ActionDescriber {
  constructor(label, callback, options) {
    this.label = label;
    this.callback = callback;
    this.options = options;
  }
}

function getEligibleActions(game, hasTurn, actions) {
  if (!hasTurn) {
    return [];
  }
  switch (game.phase) {
    case 'Calling':
    case 'Betting': {
      const base = [matchAction, raiseAction, foldAction]
      if (game.round >= 4 && game.phase !== 'Calling') {
        base.push(callAction);
      }
      return base.map(action => actions[action]);
    }
    case 'Dealing':
      return [drawAction, standAction, tradeAction].map(action => actions[action]);
    default:
      return [];
  }
}

function getEligibleInterferences(game, actions) {
  switch (game.phase) {
    case 'Calling':
    case 'Betting':
    case 'Dealing':
      return [putInFieldAction, removeFromFieldAction].map(action => actions[action]);
    default:
      return [];
  }
}

const GameActions = () => {
  const { user } = useAuth();
  const hasTurn = useSelector(isCurrentPlayer(user));
  const game = useSelector(getCurrentGame);
  const putInFieldSelected = useSelector(isPutInInterferenceFieldOngoing);
  const removeFromFieldSelected = useSelector(isRemoveFromInterferenceFieldOngoing);
  const tradeSelected = useSelector(isTradeOngoing);
  const dispatch = useDispatch();
  const disabledOption = {
    disabled: putInFieldSelected || removeFromFieldSelected || tradeSelected,
  };
  const actions = getEligibleActions(game, hasTurn, {
    [callAction]: new ActionDescriber('action.call', () => dispatch(callHand(user, game)), disabledOption),
    [drawAction]: new ActionDescriber('action.draw', () => dispatch(drawCard(user, game)), disabledOption),
    [foldAction]: new ActionDescriber('action.fold', () => dispatch(fold(user, game)), disabledOption),
    [matchAction]: new ActionDescriber('action.match.bet', () => dispatch(matchBet(user, game)), disabledOption),
    [raiseAction]: new ActionDescriber('action.raise', () => dispatch(initiateRaiseBet()), disabledOption),
    [standAction]: new ActionDescriber('action.stand', () => dispatch(stand(user, game)), disabledOption),
    [tradeAction]: new ActionDescriber(
      'action.trade',
      () => dispatch(tradeSelected ? cancelAction() : initiateTrade()),
      {
        disabled: removeFromFieldSelected || putInFieldSelected,
        selected: tradeSelected,
      }
    ),
  });
  const interferenceActions = getEligibleInterferences(game, {
    [putInFieldAction]: new ActionDescriber(
      'action.put.in.interference.field',
      () => dispatch(putInFieldSelected ? cancelAction() : initiatePutInField()),
      {
        disabled: removeFromFieldSelected || tradeSelected,
        selected: putInFieldSelected,
      }
    ),
    [removeFromFieldAction]: new ActionDescriber(
      'action.remove.from.interference.field',
      () => dispatch(removeFromFieldSelected ? cancelAction() : initiateRemoveFromField()),
      {
        disabled: putInFieldSelected || tradeSelected,
        selected: removeFromFieldSelected,
      }
    ),
  });

  return (
    <Stack style={{columnGap: '6pt'}}>
      {actions.length > 0 && (
        <>
          <Divider marginless>action.list</Divider>
          {actions.map(a => (
            <Button
              key={a.label}
              onClick={a.callback}
              disabled={a.options.disabled}
              selected={a.options.selected}
            >
              {a.label}
            </Button>
          ))}
        </>
      )}
      {interferenceActions.length > 0 && (
        <>
          <Divider marginless>action.interference</Divider>
          {interferenceActions.map(a => (
            <Button
              key={a.label}
              onClick={a.callback}
              disabled={a.options.disabled}
              selected={a.options.selected}
            >
              {a.label}
            </Button>
          ))}
        </>
      )}
      {game.isInRevealPhase()
      ? (
          <>
            <Divider marginless>waiting.reveal</Divider>
            <Button
              onClick={() => dispatch(readyForReveal(game))}
              primary
            >
              ready
            </Button>
          </>
      ) : (
        undefined
      )}
      {game.isInSuddenDemisePhase()
      ? (
          <>
            <Divider marginless>waiting.sudden.demise</Divider>
            <Button
              onClick={() => dispatch(readyForSuddenDemise(game))}
              primary
            >
              ready
            </Button>
          </>
      ) : (
        undefined
      )}
    </Stack>
  );
};

export default GameActions;
