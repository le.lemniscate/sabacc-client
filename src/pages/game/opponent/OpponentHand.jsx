import React from 'react';
import styled from 'styled-components';

import Opponent from '../../../model/Opponent';
import { FlippedCardView } from '../misc/CardView';

const CardHand = styled.div`
  > img {
    margin-left: -78pt;
  }
`;

const OpponentHand = ({ opponent }) => (
  <CardHand>
    {new Array(opponent.handSize).fill(0).map((_, i) => {
      const key = `opponent_${opponent.id}_card_{${i}}`;
      return (
        <FlippedCardView key={key} />
      );
    })}
  </CardHand>
);

OpponentHand.propTypes = {
  opponent: Opponent.shape.isRequired,
};

export default OpponentHand;
