import React from 'react';
import PropTypes from 'prop-types';
import { uuid } from 'uuidv4';

import Opponent from '../../../model/Opponent';
import OpponentView from './OpponentView';
import Columns from '../../../widgets/Columns';
import PlayerView from '../player/PlayerView';

const OpponentsList = ({ opponents, visible }) => (
  <Columns>
    {opponents.map(opponent => (
      visible // TODO: we cannot use the PlayerView (it has some kind of actions inside it)
      ? (<PlayerView key={uuid()} player={opponent} />)
      : (<OpponentView key={uuid()} opponent={opponent} />)
    ))}
  </Columns>
);

OpponentsList.propTypes = {
  opponents: PropTypes.arrayOf(Opponent.shape).isRequired,
  visible: PropTypes.bool,
};

OpponentsList.defaultProps = {
  visible: false,
};

export default OpponentsList;
