import React from 'react';

import Opponent from '../../../model/Opponent';
import OpponentInfo from './OpponentInfo';
import InterferenceField from '../misc/InterferenceField';
import OpponentHand from './OpponentHand';
import Tile from '../../../widgets/Tile';
import Column from '../../../widgets/Column';

const OpponentView = ({ opponent }) => (
  <Column>
    <Tile ancestor vertical>
      <OpponentInfo opponent={opponent} />
      <Tile child>
        <Tile parent>
          <Tile child>
            <OpponentHand opponent={opponent} />
          </Tile>
          <Tile child>
            <InterferenceField cards={opponent.interferenceField} />
          </Tile>
        </Tile>
      </Tile>
    </Tile>
  </Column>
);

OpponentView.propTypes = {
  opponent: Opponent.shape.isRequired,
};

export default OpponentView;
