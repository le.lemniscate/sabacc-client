import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';

import Opponent from '../../../model/Opponent';
import Heading from '../../../widgets/Heading';
import { isCurrentPlayer } from '../../../store/game/gameSelectors';
import PlayerName from '../misc/PlayerName';

const OpponentInfo = ({ opponent }) => {
  const hasTurn = useSelector(isCurrentPlayer(opponent));
  return (
    <div>
      <PlayerName name={opponent.name} isPlayerTurn={hasTurn} />
      <Heading>
        <span>
          <FormattedMessage
            id={`status.${opponent.status.value.toLowerCase()}`}
          />
        </span>
        <span style={{margin: '6pt'}}>-</span>
        <span>
          <i className="swg swg-credits" />
          <FormattedMessage
            id="opponent.credits"
            values={{
              amount: opponent.credits
            }}
          />
        </span>
      </Heading>
    </div>
  );
};

OpponentInfo.propTypes = {
  opponent: Opponent.shape.isRequired,
};

export default OpponentInfo;
