import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { isRaiseBetOngoing } from '../../../store/interactions/interactionSelectors';
import ModalCard from '../../../widgets/ModalCard';
import NumberInput from '../../../widgets/NumberInput';
import { getCurrentGame } from '../../../store/game/gameSelectors';
import { cancelAction, raiseBet } from '../../../store/game/gameAction';
import { useAuth } from '../../../hooks/AuthProvider';

const RaiseBet = () => {
  const isRaising = useSelector(isRaiseBetOngoing);
  const user = useAuth();
  const game = useSelector(getCurrentGame);
  const [raiseAmount, setRaiseAmount] = useState(game.currentBet + 1);
  const dispatch = useDispatch();
  return (
    <ModalCard
      isActive={isRaising}
      title="raise.bet.target"
      onValidate={() => dispatch(raiseBet(user, game, raiseAmount))}
      onClose={() => dispatch(cancelAction())}
    >
      <NumberInput
        min={game.currentBet  + 1}
        onChange={v => setRaiseAmount(v)}
      />
    </ModalCard>
  )
};

export default RaiseBet;
