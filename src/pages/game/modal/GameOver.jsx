import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import PlayerCards from '../player/PlayerCards';
import Modal from '../../../widgets/Modal';
import { getRevealedGame } from '../../../store/game/gameSelectors';
import { isGameOver } from '../../../store/interactions/interactionSelectors';
import { cancelAction } from '../../../store/game/gameAction';

const GameOver = () => {
  const isActive = useSelector(isGameOver);
  const revealedGame = useSelector(getRevealedGame);
  const history = useHistory();
  const dispatch = useDispatch();
  return (
    <Modal
      isActive={isActive}
      title="game.over"
      onValidate={() => {
        dispatch(cancelAction());
        history.push('/');
      }}
      onCancel={() => {
        dispatch(cancelAction());
        history.push('/');
      }}
    >
      {revealedGame.opponents.map(opponent => (
        <PlayerCards player={opponent} />
      ))}
    </Modal>
  )
};

export default GameOver;
