import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import GameInfo from './GameInfo';
import DeckView from './misc/DeckView';
import PlayerView from './player/PlayerView';
import OpponentsList from './opponent/OpponentsList';
import { getCurrentGame, getRevealedGame } from '../../store/game/gameSelectors';
import GameActions from './GameActions';
import { loadGame } from '../../store/game/gameAction';
import { useAuth } from '../../hooks/AuthProvider';
import Loader from '../../widgets/Loader';
import Tile from '../../widgets/Tile';
import Level from '../../widgets/Level';
import Divider from '../../widgets/Divider';
import RaiseBet from './modal/RaiseBet';

const GameView = () => {
  const game = useSelector(getCurrentGame);
  const revealedGame = useSelector(getRevealedGame);
  const { id } = useParams();
  const { user } = useAuth();
  const dispatch = useDispatch();
  if (game === null) {
    return (<Loader load={() => dispatch(loadGame(user, id))} />);
  }
  return (
    <div>
      <RaiseBet />
      <GameInfo game={game} />
      <Tile ancestor>
        <Level>
          <Tile vertical>
            <Divider marginless>deck.title</Divider>
            <DeckView size={game.deckSize} />
          </Tile>
        </Level>
        <Tile parent vertical>
          <Tile child>
            <PlayerView player={game.player} />
          </Tile>
          <Tile child>
            <Divider>opponents.title</Divider>
            { revealedGame === null
            ? (<OpponentsList opponents={game.opponents} />)
            : (<OpponentsList opponents={revealedGame.opponents} visible />)
            }
          </Tile>
        </Tile>
        <GameActions />
      </Tile>
    </div>
  );
};

export default GameView;
