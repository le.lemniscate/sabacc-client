import React, { useState } from 'react';

import { TextInput } from '../../widgets/Input';
import Button from '../../widgets/Button';
import { useAuth } from '../../hooks/AuthProvider';

const Login = () => {
  const [username, setUsername] = useState(undefined);
  const { login } = useAuth()
  return (
    <div className="container">
      <TextInput
        placeholder="player.name"
        onChange={setUsername}
      />
      <Button
        onClick={() => login(username)}
      >
        login
      </Button>
    </div>
  );
}

export default Login;
