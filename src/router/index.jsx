import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';

import Home from '../pages/home';
import Game from '../pages/game';
import Lobby from '../pages/lobby';
import Settings from '../pages/settings';
import { useAuth } from '../hooks/AuthProvider';
import Login from '../pages/login';
import CreateLobby from '../pages/lobby/CreateLobby';
import GameLoading from '../pages/game/GameLoading';
import SabaccEventReceiver from '../services/SabaccEventReceiver';
import ws from '../services/WebSocketService';

const SabaccRouter = () => {
  const { user } = useAuth()
  if (user) {
    return (
      <Router>
        <SabaccEventReceiver webSocketService={ws}>
          <Switch>
            <Route path="/game/:id">
              <Game />
            </Route>
            <Route path="/game">
              <GameLoading />
            </Route>
            <Route path="/settings">
              <Settings />
            </Route>
            <Route path="/lobby/create">
              <CreateLobby />
            </Route>
            <Route path="/lobby/:id">
              <Lobby />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </SabaccEventReceiver>
      </Router>
    );
  }
  return (
    <Login />
  );
};

export default SabaccRouter;
